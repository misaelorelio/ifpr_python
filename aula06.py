#Faça um programa que peça um número e apresente na tela o antecessor e o sucessor dele.

""" numero = float(input("Informe o número?"))
print("Antecessor", numero - 1.0)
print("O número", numero)
print("Sucessor", numero + 1.0) """

#Faça um programa que peça dois números e imprima a soma deles.

""" n1 = float(input("numero 1: "))
n2 = float(input("numero 2: "))
n3 = n1 + n2
print("Soma", n3) """

#Faça um programa que peça três números e imprima o produto (multiplicação) deles.

""" n1 = float(input("numero 1: "))
n2 = float(input("numero 2: "))
n3 = float(input("numero 3: "))
multiplicacao = n1 * n2 * n3
print(multiplicacao) """

#Faça um programa que peça as 4 notas bimestrais e mostre a média aritmética.

""" nota1 = float(input("nota 1: "))
nota2 = float(input("nota 2: "))
nota3 = float(input("nota 3: "))
nota4 = float(input("nota 4: "))
media = (nota1 + nota2 + nota3 + nota4) / 4
print("Media das notas é: ", media) """

#Faça um programa que converta metros para centímetros.

""" metros = float(input("Informe a quantidade de metros: "))
centimetros = metros * 100
print("Valor em centimetros", centimetros,"centímetros") """

#Faça um programa que peça o raio de um círculo, calcule e mostre sua área.

""" pi = 3.14
n1 = float(input("Informe o raio: "))
raio = n1 * n1
area = 3.14 * raio
print("A área desse circulo é ", area) """

# Faça um programa que peça a medida do lado de um quadrado, calcule e mostre sua área. Em seguida, mostre também o perímetro do quadrado para o usuário.

""" medida = float(input("Valor do lado do quadrado: "))
area = medida * medida
print("Área do quadraro: ", area)
print("O perímetro é de ", medida * 4) """

#Faça um programa que pergunte quanto você ganha por hora e o número de horas trabalhadas no dia. 
# Calcule e mostre quanto você ganhou no dia e o total do seu salário em um mês (considerando 30 dias).

""" valor_horas = float(input("Valor que voce ganha por hora: "))
horas_trab_dia = float(input("Horas trabalhadas por dia: "))
ganho_dia = valor_horas * horas_trab_dia
ganho_mes = ganho_dia * 30
print("Ganhos por dia: R$", ganho_dia)
print("Ganho no mês: R$", ganho_mes) """

#Faça um Programa que peça a temperatura em graus Farenheit, transforme e mostre a temperatura em graus Celsius.

""" farenheit = float(input("Informe a quantidade de graus: "))
celcius = (5 * (farenheit - 32)) / 9
print(celcius) """

#Tendo como dados de entrada a altura e peso de uma pessoa, construa um algoritmo que calcule seu IMC, usando a seguinte fórmula: imc = peso ÷ altura²

""" altura = float(input("Informe a altura em metros: "))
peso = float(input("Informe o peso: "))
imc = peso / (altura * altura)
print("Seu IMC é: ", imc) """

# Dada a equação: ax2+bx+c=0peça para o usuário informar o valor de a, b e c e calcule:


""" a = float(input("Informe o valor de a: "))
b = float(input("Informe o valor de b: "))
c = float(input("Informe o valor de c: "))
delta_de_b = (b * b) 
delta_a_c = (4 * a) * c
delta = delta_de_b - delta_a_c
print(delta)

negativo_b = (-b) - delta
x_negativo = negativo_b / (2 * a)
positivo_b = (-b) + delta
x_positivo = positivo_b / (2 * a)

print("X linha 1:", x_negativo)
print("X linha 2:", x_positivo) """

#Faça um programa em Python que receba o nome, o desconto (%) e o valor de um produto. 
# Em seguida, apresente o nome, o valor atual, o desconto (em reais) e o valor final do produto da seguinte maneira

""" nome = input("Nome do produto: ")
preco = float(input("Preço do produto: "))
desconto = float(input("Desconto do produto: "))

valor_desconto = preco * desconto / 100

print("Nome do produto: ", nome)
print("Preço do produto: R$", preco)
print("Desconto R$ ", valor_desconto)
print("Valor final do produto R$", preco - valor_desconto) """


#Faça um programa para uma loja de tintas. O programa deverá pedir o tamanho em metros
#quadrados da área a ser pintada. Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18 litros, 
#que custam R$120,00. O programa deve informar ao usuário a quantidade de latas de tinta a serem compradas e o preço total.

""" area = float(input("Tamanho da área a ser pintada: "))
quantidade_tinta = area / 54
preco = quantidade_tinta * 120.0

print("Quantidade de latas: ", quantidade_tinta)
print("Preço total R$", preco) """

#Faça um Programa que pergunte quanto você ganha por hora e o número de horas trabalhadas no mês. 
# Calcule e mostre o total do seu salário no referido mês (salário bruto), sabendo-se que são descontados 7.5% para o Imposto de Renda, 8% para o INSS e 1% para o sindicato. 
# Faça um programa que nos dê:
