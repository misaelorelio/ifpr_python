""" print("Início")
inicio = int(input("Informe o valor inicial "))
ultimo = int(input("Informe o valor final "))


for i in range(inicio, ultimo+1):
    print(i, "Eu devo estudar mais")
print("fim") """

##Tabuada em 0 ao 10 para x
"""valor = int(input("Informe o valor "))

for i in range(0, 11):
    a = i * valor
    print(a, "X", i, "= ", a)
print("---------------------") """

##Apresente os números de 1 a 100 (um por linha).

""" for i in range(1 , 101):
    print("Números", i) """

##Apresente os números de 100 a 1 (um por linha).

""" for i in range(100 , 0, -1):
    print(i) """

##Apresente os números pares de 1 a 100 (um por linha).

""" for i in range(1 , 101):
    if(i % 2 == 0):
        print("Números pares ",i) """
    
##Apresente os números ímpares de 1 a 100 (um por linha).

""" for i in range(1 , 101):
    if(i % 2 != 0):
        print("Números ímpares ",i) """

##Faça a soma dos números de 1 a 100 e ao final mostre apenas a soma total.
""" s = 0
for i in range(1 , 101):
    s += i
print(s)  """

##Faça a soma dos números de X a Y (informados pelo usuário), desde que X seja menor que Y, 
#e apresente o valor total (semelhante ao anterior).

""" valor1 = int(input("Insira um valor: "))
valor2 = int(input("Insira outro valor menor que o anterior: "))

soma = 0
if(valor1 > valor2):
    for i in range(valor2 , valor1):
        soma += i
else:
    print("Erro, informe corretamente")
print("Soma total ",soma) """

#Faça a multiplicação dos números de 1 a j (fatorial) e mostre o resultado final. 
#Exemplo: Se j = 5 você deve calcular 1 * 2 * 3 * 4 * 5 = 120

j = 5
h = 1

for i in range(j, 1, -1):
    h *= i
print(h)


#Faça um programa que leia 5 números e informe apenas o maior número.


""" valor1 = int(input("Informe um valor: "))
valor2 = int(input("Informe um valor: "))
valor3 = int(input("Informe um valor: "))
valor4 = int(input("Informe um valor: "))
valor5 = int(input("Informe um valor: "))

valores = [valor1, valor2, valor3, valor4, valor5]
resultado = max(valores)
print(resultado) """

#Faça um programa que leia 5 números e informe a soma e a média dos números.
""" soma = 0
media = 0
for i in range(1, 6):
    valor = int(input("Informe um valor: "))
    soma += valor
media = soma / 5
print("Soma", soma)
print("Média", media) """

#Faça um programa que imprima na tela apenas os números ímpares entre 1 e 50.

""" for i in range(1, 51):
    if(i % 2 != 0):
        print(i) """

#O Sr. Manoel Joaquim possui uma grande loja de artigos de R$ 1,99, com cerca de 10 caixas. 
# Para agilizar o cálculo de quanto cada cliente deve pagar ele desenvolveu uma tabela que contém o número de itens que o cliente comprou e ao lado o valor da conta. 
# Desta forma a atendente do caixa precisa apenas contar quantos itens o cliente está levando e olhar na tabela de preços. 
# Você foi contratado para desenvolver o programa que monta esta tabela de preços, que conterá os preços de 1 até 50 produtos, conforme o exemplo abaixo:
""" soma = 0
valor = 1.99
for i in range(1, 51):
    soma += valor
    print(i, "- R$ ", soma) """

#Utilizando o laço de repetição for, faça um programa que apresente as tabuadas do 1 ao 10 para um número informado pelo usuário.
""" valor = int(input("Informe um valor: "))
calculo = 0
for i in range(1, 11):
    calculo = valor * i
    print(valor, " X ", i, "=", calculo ) """

#Utilizando o laço de repetição for, faça um programa que apresente as tabuadas do X a Y para um número 
# informado pelo usuário (semelhante ao anterior, porém o usuário precisa informar três números).

""" valor1 = int(input("Informe que começa a multiplicação: "))
valor2 = int(input("Informe o valor final da multiplicação: "))
valor3 = int(input("Informe o valor da multiplicação: "))
calculo = 0
for i in range(valor1, valor2):
    calculo = valor3 * i
    print(valor3, " X ", i, "=", calculo) """