""" import random
print("== Menu de Opções == ")
print("1. Gerar um número aleatório entre X e Y")
print("2. X é par ou ímpar?")
print("3. Valor R$X com Y % de desconto")

opcao = int(input("Informe uma opção do menu: "))

print("== Opção escolhida: ", opcao)

x = int(input("Informe um valor x: "))
y = int(input("Informe um valor y: "))

if(opcao == 1):
    if(x > y):
        e = x
        x = y
        y = e

    a = random.randint(x, y)
    print("Número entre x e y: ", a)
    
elif(opcao == 2):
    if(x % 2 != 0):
        print("X é impar")
    else:
        print("X é par")
elif(opcao == 3):
    desconto = (x * y) / 100
    total = x - desconto
    print(total)
else:
    print("Opção errada cabeça de melão!")  """


""" qtdMaca = int(input("Quantos de maça kg "))
qtdMorango = int(input("Quantos de morango kg "))
total = 0
pesoTotal = qtdMaca + qtdMorango
maca = 0
morango = 0
if(qtdMorango <= 5):
    morango = qtdMorango * 8.9
if(qtdMaca <= 5):
    maca = qtdMaca * 3.9
total = (maca + morango)
if(qtdMorango > 5):
    morango = qtdMorango * 7.9
elif(qtdMaca > 5):
    maca = qtdMaca * 3.5
total = maca + morango
if(pesoTotal > 8 or total > 25):
    desconto = total * 0.07
    pagar = total - desconto
    print("Pagar esse valor R$", pagar, "Desconto R$", desconto)
else:
    print("Pagar esse valor R$", total)  """
from math import floor

valor = int(input("Informe o valor R$ "))

cem = 0
cinquenta = 0
vinte = 0
dez = 0
cinco = 0
dois = 0
um = 0

count = 0
x = valor

while valor > count:

    if(x >= 100):
        cem += 1
        count += 100
        x -= 100
    elif(x >= 50 and x < 100 ):
        cinquenta += 1
        count += 50
        x -= 50
    elif(x >= 20 and x < 50 ):
        vinte += 1
        count += 20
        x -= 20
    elif(x >= 10 and x < 20):
        dez += 1
        count += 10
        x -= 10
    elif(x >= 5 and x < 10 ):
        cinco += 1
        count += 5
        x -= 5
    elif(x >= 2 and x < 5 ):
        dois += 1
        count += 2
        x -= 2
    elif(x >= 1 and x < 2 ):
        um += 1
        count += 1
        x -= 1  
    else:
        print("Erro, informe valores válidos. Se persistir entre em contato com o suporte!")

print("Notas de R$ 100 ",cem)
print("Notas de R$ 50 ", cinquenta)
print("Notas de R$ 20 ",vinte)
print("Notas de R$ 10 ", dez)
print("Notas de R$ 5 ",cinco)
print("Notas de R$ 2 ", dois)
print("Notas de R$ 1 ",um)