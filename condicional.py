#Faça um programa que peça um valor e mostre na tela se o valor é positivo ou negativo.
""" valor = float(input("Informe o valor: "))

if(valor < 0):
    print("Negativo")
else:
    print("Positivo") """

#Faça um programa que peça dois números e imprima na tela somente o maior deles.

""" num1 = float(input("Informe o valor: "))
num2 = float(input("Informe o valor: "))

if(num1 > num2):
     print("Maior", num1)
else:
     print("Maior", num2) """

#Faça um programa que peça a idade do usuário. Se ele for maior de idade, apresente a mensagem “Entrada permitida”. Se não, apresente “Entrada negada”.

""" idade = float(input("Informe a idade: "))

if(idade >= 18):
    print("Maior de idade ", idade)
else:
    print("Menor de idade", idade) """

#Crie um programa que peça uma nota de trabalho e uma de prova (as duas de 0 a 100). 
#Se a média aritmética das notas for maior ou igual a 60, escreva “Aprovado”, se não, “Reprovado”.

""" trabalho = float(input("Informe a nota do trabalho: "))
prova = float(input("Informe a nota da prova: "))

media = (trabalho + prova) / 2

if(media < 60):
    print("Reprovado")
else:
    print("Aprovado") """

#Faça um programa que peça ao aluno o conceito dele na disciplina (A, B, C ou D). 
#Se o conceito for A, B ou C apresente “Aprovado”, se não, apresente “Reprovado”.

""" conceito = input("Informe o conceito: ")

if(conceito == 'A'):
    print("Aprovado")
if(conceito == 'B'):
    print("Aprovado")
if(conceito == 'C'):
    print("Aprovado")
if(conceito == 'D'):
    print("Reprovado") """

#Faça um programa que recebe o salário do usuário. Se o usuário recebe menos que um salário mínimo, 
#apresente na tela uma mensagem informando isso a ele. Se o salário dele for maior que o salário mínimo, 
#calcule quantos salários mínimos ele ganha e informe na tela. Considere o valor de R$1.212,00 para o salário mínimo neste exercício

""" salario = float(input("Informe o salário: "))
qtd = salario/ 1212

if(salario < 1212):
    print("Você recebe menos que um salário mínimo")
else:
    print("Quantidade de salários minímos que vc recebe", qtd) """

#Faça um programa que verifique o sexo de uma pessoa. O usuário deve informar 
#‘F’ ou ‘M’ e o programa deve escrever “Feminino” ou “Masculino”, conforme a letra digitada.

""" sexo = input("Informe o seu sexo: ")

if(sexo == 'M'):
    print("Masculino")
if(sexo == 'F'):
    print("Feminino")

 """
 #Faça um programa que peça a idade do usuário. Se ele for maior de idade, peça para ele digitar o nome dele 
 #e informe a mensagem “Bem vindo Fulano”. Caso contrário, apresente “Entrada não permitida”.

""" idade = int(input("Informe sua idade: "))

if(idade >= 18):
    print("Bem vindo fulano")
else:
    print("Entrada não permitida") """

#Faça um programa que peça a idade do usuário. Se ele for maior de idade, peça o salário dele e dê um aumento de 5%, 
#apresentando na tela o valor final do salário. Se ele for menor, informe a mensagem “Cálculo não permitido”.

""" idade = int(input("Informe sua idade: "))
salario = 2000.0
aumento = salario * 0.05

if(idade >= 18):
    print("Seu salário é R$", salario + aumento)
else:
    print("Cálculo não permitido") """

#Faça um programa que peça um número inteiro ao usuário. Se ele for maior do que zero e menor que 100, apresente a mensagem 
#“Número no intervalo definido”, se não, apresente “Número fora do intervalo!”.

""" numero = int(input("Informe um número: "))

if(numero > 0 and numero < 100):
    print("Número no intervalo definido")
else:
    print("Número fora do intervalo!") """

#Faça um programa que peça um valor e informe na tela se o número é par ou ímpar.
""" numero = float(input("Informe um numero: "))

if(numero %2 == 0):
    print("Esse número é par")
else:
    print("Número impar") """

#Faça um programa que peça um número e informe se este número é múltiplo de 3.

